import React from 'react';
import Grid from '@material-ui/core/Grid';
import { getItemPosts } from './../api/contents';

import { useSelector } from "react-redux";

import { useStyles } from './../theme';
import ItemCard from './../components/cards';


function HomePage() {
    const classes = useStyles();
    const getUserData = useSelector((state) => state.userData)
    const [state, setState] = React.useState({
        item: [],
    })

    React.useEffect(() => {
        const data = {
            uid: getUserData.uid
        }

        getItemPosts(data, (callback) => {
            if (callback.status === 200) {
                setState((prevState) => ({
                    ...prevState,
                    item: callback.data
                }));
                console.log(callback.data)
            } else {
                console.log('error');
            }
        })
    }, [state.sort, getUserData])

    return (
        <Grid item xs={12}>
            <Grid
                container
                justify="flex-start"
                alignContent="flex-start"
                direction="row"
                style={{ paddingTop: 20 }}
            >
                {state.item.length !== 0 ? (

                    state.item.map((item, index) => (
                        <ItemCard
                            index={item._id}
                            title={item.title}
                            description={item.description}
                            _id={item._id}
                            category={item.category}
                            type={item.type}
                        />
                    ))

                ) : "loading"}

            </Grid>
        </Grid>
    )
}

export default HomePage
