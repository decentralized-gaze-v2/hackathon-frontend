import React, { Component } from 'react';

import { BrowserRouter, Switch, Route } from "react-router-dom";

import { createStore } from "redux";
import { Provider } from "react-redux";
import allReducers from "./../redux/reducers";

import LandingPage from "./LandingPage";
import HomePage from "./HomePage";
import ItemPage from "./ItemPage";
import LoginPage from "./LoginPage";
import Header from "./../components/headers";
import Mount from "./../components/mount"


const store = createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);



export default class MainPage extends Component {

    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Mount />
                    <Header />
                    <Switch>
                        <Route exact path="/" component={LandingPage} />
                        <Route exact path="/Login" component={LoginPage} />
                        <Route exact path="/Home" component={HomePage} />
                        <Route exact path="/Item/:ItemNumber" component={ItemPage} />
                    </Switch>
                </BrowserRouter>
            </Provider>
        )
    }
}
