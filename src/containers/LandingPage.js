import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CardActionArea from '@material-ui/core/CardActionArea';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import HomeSearch from './../components/homeSearch';
import Footer from './../components/footers';

import { useStyles } from './../theme';
import { useTheme } from '@material-ui/core/styles';
import Placeholder from './../assets/img/itemPlaceHolder.jpg';
import firstImage from './../assets/img/2276.jpg';

function LandingPage() {

  const theme = useTheme();
  const classes = useStyles();
  const smallDevice = useMediaQuery(theme.breakpoints.down("sm"));


  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="flex-start"
        className={
          !smallDevice ? classes.bannerContainerMd : classes.bannerContainerSm
        }
      >
        <Grid item xs={10} md={10} lg={10} className={classes.paddingBetween}>
          <Grid item xs={12} md={6}>
            <br />
            <br />
            <br />
            <Grid item xs={8} md={11}>
              <Typography variant="h5" gutterBottom>
                <b>
                  A Community for Lenders and Borrowers to rent or lend help to others
                </b>
              </Typography>
            </Grid>
            <br />
            <HomeSearch />
          </Grid>
        </Grid>
      </Grid>
      <Grid
        container
        alignContent="center"
        justify="center"
        direction="row"
        className={classes.marginTopEight}
      >
        <Grid
          item
          xs={10}
          sm={10}
          md={8}
          lg={5}
          className={classes.paddingBetween}
          style={{ marginBottom: theme.spacing(1) }}
        >
          <CardMedia
            className={classes.landingImage}
            image={firstImage}
            title="Designed by pressfoto / Freepik"
          />
        </Grid>
        <Grid
          item
          xs={10}
          sm={10}
          md={3}
          lg={5}
          className={classes.paddingBetween}
        >
          <Typography variant="h5" color="textPrimary" gutterBottom>
            <b>Why find items in Pahiram?</b>
          </Typography>
          <Typography variant="body2" component="p" gutterBottom>
            Pahiram is ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum accumsan fringilla erat eget consequat. Duis molestie enim felis, nec imperdiet libero faucibus non. Nulla quis suscipit eros, at accumsan sem. Praesent ac tincidunt erat. Morbi et elementum neque. Duis pharetra mollis velit, vel consequat nunc vehicula quis.
          </Typography>
          <Grid
            container
            direction="column"
            style={{ marginTop: theme.spacing(1) }}
          >
            <Grid
              container
              direction="row"
              alignContent="flex-start"
              style={{ flex: 1 }}
            >
              <Grid item className={classes.paddingBetweenOne}>
              </Grid>
              <Grid item className={classes.paddingBetweenOne}>
              </Grid>
            </Grid>
            <Grid
              container
              direction="column"
              alignContent="flex-start"
              justify="center"
              style={smallDevice ? { height: "4rem" } : { height: "6vw" }}
            >
              <Grid item>
                <Button color="primary" variant="outlined" size="large">
                  <b>Explore pahiram</b>
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        container
        alignContent="center"
        justify="center"
        direction="row"
        className={classes.marginTopEight}
      >
        <Grid
          item
          xs={10}
          sm={10}
          md={10}
          lg={10}
          className={classes.paddingBetween}
        >
          <Typography variant="h5" color="textPrimary" gutterBottom>
            <b>Explore veriety of items in Pahiram</b>
          </Typography>
        </Grid>
        <Grid item xs={10} sm={10} md={10} lg={10}>
          <Grid
            container
            justify="flex-start"
            alignContent="flex-start"
            direction="row"
          >
            <Grid
              item
              xs={12}
              sm={6}
              md={6}
              lg={4}
              className={classes.paddingTwo}
            >
              <Card className={classes.landingCardRoot}>
                <CardHeader
                  action={
                    <IconButton aria-label="settings">
                      <MoreVertIcon />
                    </IconButton>
                  }
                  title={
                    <Typography variant="body2">
                      <b>Ps4 Game</b>
                    </Typography>
                  }
                  subheader={
                    <Grid
                      item
                      xs={12}
                      md={6}
                      className={classes.landingSubheader}
                    >
                      <Typography variant="caption" noWrap>
                        Video Game
                      </Typography>
                    </Grid>
                  }
                />
                <CardActionArea >
                  <CardMedia
                    className={classes.mediaCard}
                    image={Placeholder}
                    title="Item"
                  />
                  <CardContent>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      Loreem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum accumsan fringilla erat eget consequat. Duis molestie enim felis, nec imperdiet libero faucibus non. Nulla quis suscipit eros, at accumsan sem. Praesent ac tincidunt erat. Morbi et elementum neque. Duis pharetra mollis velit
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions disableSpacing>
                  <IconButton aria-label="share">
                    <ShareIcon />
                  </IconButton>
                  <Button size="medium" >
                    Go to Item
                  </Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid
              item
              xs={12}
              sm={6}
              md={6}
              lg={4}
              className={classes.paddingTwo}
            >
              <Card className={classes.landingCardRoot}>
                <CardHeader
                  action={
                    <IconButton aria-label="settings">
                      <MoreVertIcon />
                    </IconButton>
                  }
                  title={
                    <Typography variant="body2">
                      <b>Piano Keyboard</b>
                    </Typography>
                  }
                  subheader={
                    <Grid
                      item
                      xs={12}
                      md={6}
                      className={classes.landingSubheader}
                    >
                      <Typography variant="caption" display="block" noWrap>
                        Hobbies
                      </Typography>
                    </Grid>
                  }
                />
                <CardActionArea >
                  <CardMedia
                    className={classes.mediaCard}
                    image={Placeholder}
                    title="item"
                  />
                  <CardContent>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      Loreem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum accumsan fringilla erat eget consequat. Duis molestie enim felis, nec imperdiet libero faucibus non. Nulla quis suscipit eros, at accumsan sem. Praesent ac tincidunt erat. Morbi et elementum neque. Duis pharetra mollis velit
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions disableSpacing>
                  <IconButton aria-label="share">
                    <ShareIcon />
                  </IconButton>
                  <Button
                    size="medium"

                  >
                    Go to Item
                  </Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid
              item
              xs={12}
              sm={6}
              md={6}
              lg={4}
              className={classes.paddingTwo}
            >
              <Card className={classes.landingCardRoot}>
                <CardHeader
                  action={
                    <IconButton aria-label="settings">
                      <MoreVertIcon />
                    </IconButton>
                  }
                  title={
                    <Typography variant="body2">
                      <b>Laptop</b>
                    </Typography>
                  }
                  subheader={
                    <Grid
                      item
                      xs={12}
                      md={6}
                      className={classes.landingSubheader}
                    >
                      <Typography variant="caption" display="block" noWrap>
                        Gadget
                      </Typography>
                    </Grid>
                  }
                />
                <CardActionArea >
                  <CardMedia
                    className={classes.mediaCard}
                    image={Placeholder}
                    title="Item"
                  />
                  <CardContent>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      Loreem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum accumsan fringilla erat eget consequat. Duis molestie enim felis, nec imperdiet libero faucibus non. Nulla quis suscipit eros, at accumsan sem. Praesent ac tincidunt erat. Morbi et elementum neque. Duis pharetra mollis velit
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions disableSpacing>
                  <IconButton aria-label="share">
                    <ShareIcon />
                  </IconButton>
                  <Button
                    size="medium"

                  >
                    Go to Item
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        justify="center"
        className={
          smallDevice
            ? classes.landingBannerBottomSm
            : classes.landingBannerBottomMd
        }
      >
        <Grid
          item
          xs={11}
          md={10}
          lg={9}
          className={
            smallDevice
              ? classes.bottomBannerContainerSm
              : classes.bottomBannerContainerMd
          }
        >
          <Grid
            item
            className={
              smallDevice
                ? classes.bottomBannerContentSm
                : classes.bottomBannerContentMd
            }
          >
            <Typography variant="h5" style={{ color: "white" }}>
              <b>Help our community!</b>
            </Typography>
            <Typography
              variant="h5"
              component="h4"
              style={{ color: "white" }}
              gutterBottom
            >
              Borrow or Lend Item you need now
            </Typography>
            <Grid
              container
              direction="row"
              justify={smallDevice ? "center" : "flex-end"}
              style={{ marginTop: theme.spacing(3) }}
            >
              <Button
                color="primary"
                variant="contained"
                size="large"
                fullWidth={smallDevice ? true : false}
              >
                <b>Register and get started</b>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Footer />
    </div>
  )
}

export default LandingPage;
