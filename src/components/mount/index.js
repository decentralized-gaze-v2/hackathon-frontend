import React from 'react';
import { collectUserData } from "./../../redux/actions";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

// this mounts the local storage to redux

export default function ItemCard() {

    const history = useHistory();
    const dispatch = useDispatch();
    let userSession = JSON.parse(localStorage.getItem('user_data_session'))

    React.useEffect(() => {
        dispatch(collectUserData(userSession));

        if (userSession !== undefined) {
            history.push('/')
        }

    }, [dispatch, userSession, history])


    return null;

}