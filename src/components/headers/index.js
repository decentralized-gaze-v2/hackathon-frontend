import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function ButtonAppBar() {
    const classes = useStyles();
    const history = useHistory();

    return (
        <div className={classes.root}>
            <AppBar position="sticky" style={{ backgroundColor: "#FFF", color: "#000" }}>
                <Toolbar style={{ height: "10vh" }}>
                    <Typography variant="h6" className={classes.title} style={{ paddingLeft: "20px" }}>
                        <b>Pah<span style={{ color: 'blue' }}>i</span>ram</b>
                    </Typography>
                    <Button color="inherit" onClick={() => history.push('/login')}>Login</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}