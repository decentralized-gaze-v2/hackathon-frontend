import React, { useState } from "react";

import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { Autocomplete } from "@material-ui/lab";

import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import HelpIcon from "@material-ui/icons/Help";

import { categories } from "../constants/categories";
import { items } from "../constants/items";

import { useStyles } from "./../theme";

function ItemFinder() {
  const classes = useStyles();
  const [values, setValues] = useState({
    product: "Laptop",
    category: {},
    items: "Ps4 Games"
  });

  const handleChange = (prop) => (event) => {
    setValues({
      ...values,
      [prop]: event.target.value,
    });
  };

  return (
    <Card>
      <CardContent style={{ minHeight: "32vh", maskImage: "none" }}>
        <Typography variant="body2" color="textPrimary">
          <b>Pahiram Item Finder</b>
        </Typography>
        <Typography variant="body2" component="p">
          Lending or Borrowing Item never gets easier
        </Typography>
        <br />
        <Autocomplete
          freeSolo
          id="search"
          value={values.items}
          onChange={handleChange("items")}
          disableClearable
          options={items.map((option) => option.product)}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Search an Item"
              margin="normal"
              variant="outlined"
              InputProps={{ ...params.InputProps, type: "search" }}
            />
          )}
        />
        <div className={classes.marginTopTwo}>
          <Autocomplete
            multiple
            id="tags-outlined"
            options={categories}
            getOptionLabel={(option) => option.category}
            defaultValue={[categories[1]]}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Desired Category"
                placeholder="Categories"
              />
            )}
          />
        </div>
      </CardContent>
      <CardActions
        style={{
          justifyContent: "space-between",
          flexDirection: "row",
          marginBottom: "16px",
          paddingRight: "16px",
          paddingLeft: "16px",
        }}
      >
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <HelpIcon fontSize="inherit" />
        </IconButton>
        <Button color="primary" variant="contained" size="large">
          <b>Find Item now</b>
        </Button>
      </CardActions>
    </Card>
  );
}

export default ItemFinder;
