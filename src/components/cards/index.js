import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CardActionArea from '@material-ui/core/CardActionArea';
import Card from '@material-ui/core/Card';
import { useHistory } from "react-router-dom";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import { useStyles } from './../../theme';
import CardMedia from '@material-ui/core/CardMedia';


import Placeholder from './../../assets/img/itemPlaceHolder.jpg';

export default function ItemCard({ title, category, img, description, _id, index, type }) {

    const classes = useStyles();
    const history = useHistory();

    const currentPage = history.location.pathname;


    return (
        <Grid
            item
            xs={6}
            sm={6}
            md={3}
            lg={3}
            className={classes.paddingTwo}
        >
            <Card key={index} className={classes.landingCardRoot} >
                <CardHeader
                    action={
                        <IconButton aria-label="settings">
                            <MoreVertIcon />
                        </IconButton>
                    }
                    title={
                        <Typography variant="body2">
                            <b>{title}</b>
                        </Typography>
                    }
                    subheader={
                        <Grid
                            item
                            xs={12}
                            md={6}
                            className={classes.landingSubheader}
                        >
                            <Typography variant="caption" noWrap>
                                {category}
                            </Typography>
                        </Grid>
                    }
                />
                <CardActionArea >
                    <CardMedia
                        className={classes.mediaCard}
                        image={Placeholder}
                        title="Luzon Hub"
                    />
                    <CardContent>
                        <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p"
                        >
                            {description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions disableSpacing>
                    <Button size="medium" onClick={() => {
                        history.push(currentPage + `/${_id}`)
                    }}>
                        View Item
                  </Button>
                </CardActions>
            </Card>
        </Grid >
    );
}