import axios from "axios";
import { server } from "./index";


export async function loginUser(data, callback) {
    await axios
        .post(`${server}auth/login`, data)
        .then((response) => {
            callback(response);
        })
        .catch((error) => {
            callback(error.response);
        });
}
