import axios from "axios";
import { server } from "./index";

//param : uid
export async function getItemPosts(data, callback) {
    await axios
        .post(`${server}item/all`, data)
        .then((response) => {
            callback(response);
        })
        .catch((error) => {
            callback(error.response);
        });
}
