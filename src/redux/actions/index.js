import {
    COLLECT_USER_DATA,
    RESET_DATA,
} from "./actionTypes";
  
  // for User Data after Authenication
  export function collectUserData(data) {
    return {
      type: COLLECT_USER_DATA,
      payload: data,
    };
  }
  // end
  
  // for state reset
  export function resetData() {
    return {
      type: RESET_DATA,
    };
  }
  