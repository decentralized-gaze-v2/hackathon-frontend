import { createMuiTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";

import bottomBanner from "./../assets/img/174711.png";
import illustration from './../assets/img/undraw_dreamer_gxxi.svg';

export const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 730,
      md: 800,
      lg: 990,
      xl: 1200,
    },
  },
  palette: {
    primary: {
      main: "#e25b28",
      contrastText: "#fff",
    },
    secondary: {
      main: "#2E70D2",
      contrastText: "#fff",
    },
  },
});

export const useStyles = makeStyles((theme) => ({
  //Central Styles
  paddingBetween: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  paddingBetweenOne: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  paddingTwo: {
    padding: theme.spacing(2),
  },

  marginTopTwo: {
    marginTop: theme.spacing(2),
  },
  marginTopTwoo: {
    marginTop: theme.spacing(2),
  },
  marginTopFive: {
    marginTop: theme.spacing(5),
  },
  marginTopEight: {
    marginTop: theme.spacing(8),
  },
  mediaCard: {
    height: 0,
    paddingTop: "73.45%",
  },
  toLowerCase: {
    textTransform: "lowercase",
  },
  toCapitalized: {
    textTransform: "capitalize",
  },
  cardHover: {
    borderShadow: "none",
    transition: "box-shadow 300ms",
    "&:hover": {
      boxShadow: "0 23px 17px 0 rgba(0, 0, 0, 0.13)",
    },
  },
  //footer
  footer: {
    padding: theme.spacing(3, 2),
    marginTop: "3em",
    // backgroundColor:
    //   theme.palette.type === "light"
    //     ? theme.palette.grey[200]
    //     : theme.palette.grey[800],
    backgroundColor: "#393939",
    color: "#FFF",
  },
  zeendLink: {
    color: "green",
  },
  footerLogo: {
    height: "2rem",
    borderRadius: "12px",
    marginBottom: ".5em",
  },
  airlogo: {
    height: "2.5rem",
    width: "7rem",
    borderRadius: "4px",
    marginBottom: ".5em",
  },
  zeendFooterLogo: {
    height: "2.2rem",
    borderRadius: "4px",
    marginBottom: ".5em",
    marginLeft: "1em",
  },
  paymayaLogo: {
    height: "2.5rem",
    width: "7em",
    borderRadius: "12px",
    marginBottom: ".5em",
  },
  grabLogo: {
    height: "2.5rem",
    width: "7em",
    borderRadius: "12px",
    marginBottom: ".5em",
  },
  alafopLogo: {
    height: "3rem",
    width: "6em",
    borderRadius: "12px",
    marginBottom: ".5em",
  },
  unionbankLogo: {
    height: "2rem",
    width: "7em",
    borderRadius: "12px",
    marginBottom: ".5em",
    marginLeft: "2em",
  },
  //header
  grow: {
    flexGrow: 1,
  },
  // productSearcgBar: {
  //   padding: "2px 4px",
  //       display: "flex",
  //       alignItems: "center",
  // },
  titleHeader: {
    transition: "all .3s ease",
    [theme.breakpoints.up("sm")]: {
      display: "block",
      marginLeft: theme.spacing(2),
    },
    marginLeft: theme.spacing(1),
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  drawer: {
    width: 250,
  },
  productButton: {
    margin: "auto",
    width: "9.2rem",
    minHeight: "3rem",
  },
  buttonLinks: {
    padding: "22.5px 0 22.5px",
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    textDecoration: "none",
    letterSpacing: "0.05em",
    display: "inline-block",
    "&:hover": {
      textDecoration: "none",
      transition: "0.2s",
      boxShadow: "0 4.7px 0 0 #E25B28",
    },
    "&:link": {
      textDecoration: "none",
    },
    "&:visited": {
      textDecoration: "none",
    },
  },
  navIcons: {
    width: "48px",
    margin: "auto",
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  //formDialog
  formIconContainer: {
    padding: "1px",
  },
  formIcon: {
    padding: theme.spacing(1),
    margin: theme.spacing(1),
  },
  //login form
  loginInputMargin: {
    margin: theme.spacing(1, 0, 1),
  },
  loginButton: {
    margin: theme.spacing(1, 0, 1),
  },
  buttonSpaceBetween: {
    margin: theme.spacing(1, 0),
    justifyContent: "space-between",
  },
  homeSubheader: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    width: "10rem",
    textOverflow: "ellipsis",
  },
  homeMainCard: {
    height: "18rem",
    [theme.breakpoints.down("sm")]: {
      height: "8rem",
    },
  },
  homeMainImg: {
    height: 0,
    borderRadius: "4px",
    paddingTop: "73.45%",
  },
  makatiMain: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  //LandingPage
  landingCardRoot: {
    flexGrow: 1,
  },
  bannerContainerMd: {
    minHeight: "80vh",
    height: "auto",
    backgroundSize: "47vw ,80vw",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "47vw 5em",
    backgroundImage: `url(${illustration})`,
    maskImage: "linear-gradient(to bottom, black 90%, transparent 100%);",
  },
  bannerContainerSm: {
    minHeight: "40vh",
    height: "auto",
    backgroundSize: "47vw",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "48vw 5em",
    backgroundImage: `url(${illustration})`,
  },
  landingBannerBottomMd: {
    marginTop: theme.spacing(20),
    position: "relative",
    height: "350px",
    backgroundColor: "#2b2d33",
    transition: "all .3s ease",
  },
  bottomBannerContainerMd: {
    height: "100%",
    backgroundImage: `url(${bottomBanner})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "-11vw 0",
    backgroundSize: "cover",
    position: "relative",
    padding: "0",
  },
  bottomBannerContentMd: {
    backgroundImage:
      "linear-gradient(90deg,#2b2d33,rgba(43,45,51,0) 10%),linear-gradient(90deg,#2b2d33,rgba(43,45,51,0) 10%),linear-gradient(70.74deg,rgba(43,45,51,0) 31.83%,#2b2d33 72.87%),linear-gradient(150.01deg,rgba(13,61,118,.5) 6.57%,rgba(13,61,118,0) 50.06%)",
    width: "100%",
    height: "100%",
    padding: "100px 0 100px 50%",
  },
  landingBannerBottomSm: {
    backgroundColor: "#2b2d33",
    marginTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    transition: "all .3s ease",
  },
  bottomBannerContainerSm: {
    width: "100%",
    marginRight: "auto",
    marginLeft: "auto",
    marginTop: theme.spacing(4),
  },
  landingImage: {
    height: 0,
    paddingTop: "73.45%",
    borderRadius: "4px",
  },
  landingSubheader: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    width: "10rem",
    textOverflow: "ellipsis",
  },
  paddingAround: {
    padding: "20px",
  },
  landingCard: {
    height: 0,
    borderRadius: "4px",
    paddingTop: "73.45%",
  }
}));
